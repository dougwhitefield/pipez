package pipez;

import pipez.core.Block;
import pipez.core.Pipe;

/**
 * Returns only Blocks which have at least one value which has the given string.
 * e.g. for a given CSV file, only lines which contain the given string are passed through.
 * 
 * @author TBD
 *
 */
public class ValueMatchPipe implements Pipe {

	@Override
	public String getName() {
		return "Value Match";
	}

	private String toMatch = null;

	private ValueMatchPipe(String toMatch) {
		this.toMatch = toMatch;
	}
	
	public static ValueMatchPipe create(String toMatch) {
		return new ValueMatchPipe(toMatch);
	}
	
	@Override
	public Block transform(Block block) {
		// TODO Auto-generated method stub
		return null;
	}

}
